# Vehicle Data Diagnostic

## NA
![vdd](screenshot/vdd.jpg)

## ADR
### Spec
* Firmware : 56.00.513-ADR
* Car : Mazda 2 SKYACTIV-D 105

![vdd](screenshot/vdd-7.png)

![vdd](screenshot/vdd-7-1.png)

![vdd](screenshot/vdd-7-2.png)
